import { useState } from "react"; // hook
import Splash from "./Components/SplashScreen";
import { ThemeProvider } from "styled-components"; // https://styled-components.com/docs/api#themeprovider

// tema claro
const LightTheme = {
  pageBackground: "white",
  titleColor: "#e83f0c",
  tagLineColor: "black",
};

// tema oscuro
const DarkTheme = {
  pageBackground: "#282c36",
  titleColor: "#e83f0c",
  tagLineColor: "white",
};

// constante que almacena los dos temas
const themes = {
  light: LightTheme,
  dark: DarkTheme,
};

function App() {
  const [theme, setTheme] = useState("light");
  return (
    <ThemeProvider theme={themes[theme]}>
      <Splash theme={theme} setTheme={setTheme} />
    </ThemeProvider>
  );
}

export default App;
