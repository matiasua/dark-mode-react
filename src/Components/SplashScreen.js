import styled from "styled-components";
import { CgSun } from "react-icons/cg";
import { HiMoon } from "react-icons/hi";

// contiene el style del componente boton
const Button = styled.button`
  cursor: pointer;
  height: 50px;
  width: 50 px;
  border-radius: 50%;
  border: none;
  background-color: ${(props) => props.theme.titleColor};
  color: ${(props) => props.theme.pageBackground};
  &:focus {
    outline: none;
  }
  transition: all 0.5s ease;
`;

// contiene el style del componente page
const Page = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
  background-color: ${(props) => props.theme.pageBackground};
  transition: all 0.5s ease;
`;

// contiene el style del componente container
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

// contiene el style del titulo
const Title = styled.h1`
  color: ${(props) => props.theme.titleColor};
  transition: all 0.5s ease;
`;

// contiene el style de la siguiente linea
const TagLine = styled.span`
  color: ${(props) => props.theme.tagLineColor};
  font-size: 18px;
  transition: all 0.5s ease;
`;

// funcion que recibe las props theme y setTheme
function Splash(props) {
  // funcion que cambia el tema
  function changeTheme() {
    if (props.theme === "light") {
      props.setTheme("dark");
    } else {
      props.setTheme("light");
    }
  }
  // variable para definir que icono ocupar. Se utilizo ternarios
  const icon =
    props.theme === "light" ? <HiMoon size={40} /> : <CgSun size={40} />;

  return (
    <Page>
      <Container>
        <Button onClick={changeTheme}> {icon} </Button>
        <Title> Learning React </Title>
        <TagLine>
          Hooks, Styled-Components[ThemeProvider], React-Icons,
          Tagged-templates!
        </TagLine>
      </Container>
    </Page>
  );
}

export default Splash;
